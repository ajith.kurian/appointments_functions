To add this to RStudio, start by installing and activating the devtools package 
(install command and library command). After this, go to Tools>Global Options
click on the packages tab on the left, and click add. Enter the following URL:"
https://gitlab.com/ajith.kurian/appointments_functions
" and give it a name (I use Appt), then add it. After this, you can call the 
library by using library(appt). After this the TimeAvailable(data.frame) 
function should be available and ready to use. This takes in data from the 
appointments table query (which can be found in the query folder) and converts
thestring of 0's and 1's in the booked and blocked slots into columns for each
15 minute time interval, stating "yes" if they are working for a 15 minute 
interval without any appointments. 